#!/usr/bin/python
# coding: utf-8

from __future__ import print_function
import sys, getopt
import os
import collections
import json
from pprint import pprint as pp

global apiaddress

try:
    import requests
except Exception as e:
    raise Exception("Lib requests absente - installez la avec la commande : pip install requests")

debug = False

HEADER = '\033[95m'
OKBLUE = '\033[94m'
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'
BOLD = "\033[1m"

api_header = {"Content-Type": "application/json;charset=utf-8"}
global log
log = {}
global companies
companies = {}

class RobertTable:

    def request(self, apiAddress):
        r = requests.post(
            apiAddress + self.api_point,
            data=json.dumps(self.json),
            headers=api_header)
        log_request(r, self.data_id, self.lineId, self.chunk_id)

class Tekos(RobertTable):

    id              = 0
    idUser          = 1 #
    surnom          = 2 #
    nom             = 3 #
    prenom          = 4 #
    email           = 5 #
    tel             = 6 #
    GUSO            = 7
    CS              = 8
    birthDay        = 9
    birthPlace      = 10
    habilitations   = 11
    categorie       = 12
    SECU            = 13
    SIRET           = 14
    assedic         = 15
    intermittent    = 16
    adresse         = 17 #
    cp              = 18 #
    ville           = 19 #
    diplomes_folder = 20 #

    api_point = "/persons/new/"
    chunk_id  = "tekos"

    @staticmethod
    def fromSQL(sql_line, line_nb):
        data = sql_line_to_list(sql_line)
        if len(data) != 21:
            ## bad line structure ##
            return None

        tekos = Tekos()
        tekos.lineId = line_nb
        tekos.data_id = "{} {}".format(data[Tekos.nom], data[Tekos.prenom])
        tekos.json = {
            "nickname"  : data[Tekos.surnom],
            "first_name": data[Tekos.prenom],
            "last_name" : data[Tekos.nom],
            "email"     : data[Tekos.email],
            "phone"     : data[Tekos.tel],
            "tags"      : ["operator", data[Tekos.categorie]],
            "address"   : {
                "street_address": data[Tekos.adresse],
                "postal_code"   : data[Tekos.cp],
                "city"          : data[Tekos.ville],
            },
        }

        if not tekos.json["address"]["street_address"]:
            tekos.json.pop("address")


        if int(data[Tekos.intermittent]) == 1:
            tekos.json["tags"].append("intermittent")
        return tekos

    def __str__(self):
        return "Je suis un tekos {}".format(self.data_id)


def log_request(server_response, data_id, line_id, current_chunk):
    global log
    if not current_chunk in log:
        log[current_chunk] = {
            "success" : 0,
            "exist"   : 0,
            "errors"  : 0,
            "errors_lines" : collections.defaultdict(list)
        }

    if server_response.status_code == 201:
        print ("\t{}201{} : Added {}".format(OKGREEN, ENDC, data_id))
        log[current_chunk]["success"] += 1
    elif server_response.status_code == 409:
        print ("\t{}409{} : Exist {}".format(WARNING, ENDC, data_id))
        log[current_chunk]["exist"] += 1
        # pp(server_response.json())
    elif server_response.status_code == 500:
        log[current_chunk]["errors"] += 1
        # pp(server_response.json())
        log[current_chunk]["errors_lines"][current_chunk].append((line_id, server_response.json()["error"]["message"]))
    else:
        log[current_chunk]["errors"] += 1
        log[current_chunk]["errors_lines"][current_chunk].append((line_id, server_response.json()["error"]))
        print ("\t{} : Error {}".format(FAIL + str(server_response.status_code) + ENDC, data_id))

def print_log(section):
    global log
    print ("\n\033[1;34;40m---------- {} log ----------\033[0m\n".format(section))
    print ("{:4} inserts\n{:4} duplicates\n{:4} errors".format(
        log[section]["success"],
        log[section]["exist"],
        log[section]["errors"]))

    for errline in log[section]["errors_lines"][section]:
        try:
            print ("\t l{:4} : {}".format(errline[0], ', '.join("%s=%r" % (key,val) for (key,val) in errline[1].iteritems())))
        except Exception as e:
            print ("\t l{:4} : {}".format(errline[0], " multiples errors"))
            pp(errline[1])
    print ("\n\033[1;34;40m---------- end {} log ----------\033[0m\n".format(section))

def print_all_log():
    for log_section, _ in log.items():
        print_log(log_section)


def sql_line_to_list(line):
    datas = line[1:-3].split("',")
    formatted = []
    for data in datas:
        if data == '\'':
            formatted.append(None)
        else:
            formatted.append(data[1:].replace("\\'", "'").strip())
    return formatted


def import_robert1(inputSQL):
    with open(inputSQL, "r") as f:
        raw_dump = f.read().splitlines()
    chunks = collections.OrderedDict()
    tmp_chunk = []
    current_chunk = None
    current_line = None
    multiline_append = False

    for ind, line in enumerate(raw_dump):
        # new chunk of insert
        if line.startswith("INSERT INTO "):
            if tmp_chunk:
                chunks[current_chunk] = tmp_chunk

            current_chunk = line[13:-8]
            tmp_chunk = []

        if (current_chunk
                and line.startswith("(")
                and ( line.endswith("),") or line.endswith(");") )
           ):
            #a line of INSERT begin
            current_line = line
            if debug:
                print ("l:{} FULLLINE".format(ind + 1))
            tmp_chunk.append((ind + 2, current_line))

        elif (current_chunk
                and line.startswith("(")
             ):
            if debug:
                print ("l{} BEGINS MULTI".format(ind + 1))
            current_line = line
            multiline_append = True

        elif multiline_append and ( line.endswith("),") or line.endswith(");") ):
            if debug:
                print ("l{} MULTI STOP".format(ind + 1))
            current_line += line
            multiline_append = False
            tmp_chunk.append((ind + 2, current_line))

        elif multiline_append:
            if debug:
                print ("l{} MULTI APPEND".format(ind + 1))
            current_line += line

    if tmp_chunk:
        chunks[current_chunk] = tmp_chunk
    print ("")
    for struct, datas in chunks.items():
        print ("Table {:30} with {:4} records".format(struct, len(datas)))

    print ("\n---------- Adding companies ----------")
    structures_to_companies(chunks["robert_benef_structure"])

    print ("\n---------- Adding Persons from Interlocuteurs ----------")
    interloc_to_person(chunks["robert_benef_interlocuteurs"])

    print ("\n---------- Adding Persons from Tekos ----------")
    tekos_to_person(chunks["robert_tekos"])
    print_all_log()

def interloc_to_person(interloc_chunk):
    global apiaddress
    global companies
    add_person_url = apiaddress + "/persons/new"

    for interloc_raw in interloc_chunk:

        interloc_data = interloc_raw[1]
        interloc_line = interloc_raw[0]
        person = sql_line_to_list(interloc_data)

        if len(person[3].split(" ")) == 2:
            fname, lname = person[3].split(" ")
        elif len(person[3].split(" ")) == 3:
            tmp_name = person[3].split(" ")
            fname = tmp_name[0] + " " + tmp_name[1]
            lname = tmp_name[2]
        elif len(person[3].split(" ")) == 1:
            fname = person[3]
            lname = person[3]

        if companies[person[2]]:
            company_name = companies[person[2]]
            company_id = get_company_by_name(company_name)["idcompany"]

        datas = {
            "first_name":  fname,
            "last_name" :  lname,
            "nickname"  :  person[1] if person[1] != '' else None,
            "phone"     :  person[8] if person[8] != '' else None,
            "email"     :  person[7] if person[7] != '' else None,
            "company_id": int(company_id),
            "note"      : person[10] if person[10] != '' else None,
            "address"   : {
                "street_address" : person[4] if person[4] != '' else None,
                "city"       :  person[6] if person[6] != '' else None,
                "postal_code":  person[5] if person[5] != '' else None,
            },
        }
        if person[9]:
            datas["tags"] = [person[9]]

        r = requests.post(add_person_url, data=json.dumps(datas), headers=api_header)
        log_request(r, " ".join([lname, fname]) , interloc_line, "interlocuteur")

def tekos_to_person(tekos_chunk):
    for line_id, data in tekos_chunk:
        tek = Tekos.fromSQL(data, line_id)
        if tek:
            tek.request(apiaddress)
        else:
            print("Tekos error line {}".format(line_id))

def get_company_by_name(name):
    company_search_url = apiaddress + "/companies/search" + "?legal_name=" + name
    r = requests.get(company_search_url)
    if r.status_code == 200:
        return r.json()[0]
    else:
        print ("NOT FOUND company : ")
        pp(r.json())
        return {"idcompany" : None}

def structures_to_companies(structures_chunk):
    global apiaddress
    global log
    global companies
    add_company_url = apiaddress + "/companies/new"


    for struct_raw in structures_chunk:

        struct_data = struct_raw[1]
        struct_line = struct_raw[0]
        struct = sql_line_to_list(struct_data)

        datas = {
            "legal_name" :  struct[4] if len(struct[4]) >= len(struct[1]) != '' else struct[1],
            "email"      :  struct[9] if struct[9] != '' else None,
            "phone"      : struct[10] if struct[10] != '' else None,
            "note"       : struct[14] if struct[14] != '' else None,
            "city"       :  struct[8] if struct[8] != '' else None,
            "postal_code"    : struct[7] if struct[7] != '' else None,
            "street_address" : struct[6] if struct[6] != '' else None,
        }

        companies[struct[0]] = datas["legal_name"]

        r = requests.post(add_company_url, data=json.dumps(datas), headers=api_header)
        log_request(r, datas["legal_name"], struct_line, "structures")

def main(argv):
    inputfile = ''
    global apiaddress

    try:
       opts, args = getopt.getopt(argv,"hi:a:",["ifile=","api="])
    except getopt.GetoptError:
       print ('import_robert-05_sql.py -i <SQLDump> -a <API address>')
       sys.exit(2)
    for opt, arg in opts:
       if opt == '-h':
          print ('import_robert-05_sql.py -i <SQLDump> -a <API address>')
          sys.exit()
       elif opt in ("-i", "--ifile"):
          inputfile = arg
       elif opt in ("-a", "--api"):
          apiaddress = arg

    print ('Reading SQL dump : {}'.format(inputfile), end='')
    if not os.path.isfile(inputfile):
        sys.exit("\tError, not a file")
    print (" {}OK{}".format(OKGREEN, ENDC))

    print ('Testing Api Address : {}'.format(apiaddress), end='')
    try:
        r = requests.get(apiaddress)
    except Exception as e:
        sys.exit("\tUnable to get to this address")
    print (" {}OK{}".format(OKGREEN, ENDC))

    import_robert1(inputfile)

if __name__ == "__main__":
   main(sys.argv[1:])


   # test_line = """('2','10','Guigui','FABRE','Guillaume','guillaume.acousmie@gmail.com','0677862640','202633202','C359469','1983-03-15','Gap (05)','undefined','son','183030506104136','N/A','72129679','1','Le village/place de l\'école','05400','Le Saix','GuiDiploms'),"""
   # t = Tekos.fromSQL(test_line)
   # print(t)
   # t.call()
