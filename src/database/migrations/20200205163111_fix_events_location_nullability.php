<?php
// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace

use Phinx\Migration\AbstractMigration;

class FixEventsLocationNullability extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('events');
        $table
            ->changeColumn('location', 'string', ['null' => true, 'length' => 64])
            ->update();

        $this->execute("UPDATE `events` SET `location` = NULL WHERE `location` = ''");
    }

    public function down()
    {
        $this->execute("UPDATE `events` SET `location` = '' WHERE `location` IS NULL");

        $table = $this->table('events');
        $table
            ->changeColumn('location', 'string', ['length' => 64])
            ->update();
    }
}

// phpcs:enable
